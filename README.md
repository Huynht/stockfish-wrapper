# Stockfish Wrapper

## Endpoints
- /best-move(fen: string, depth: number)
- /apply-move(fen: string, move: string)
- /is-game-over(fen: string)
- /convert-fen(oldFen: string, newPositions: number[][])
    - newPositions encoding ist 0 = empty, 1 = weiß / gelb,  2 = schwarz / blau
- /convert-positions(coordinates: string || indeces: [number, number])
    - schick entweder coordinates oder indeces und bekomme das andere zurück

## Getting started
### For preview:
```
npm install
npm run dev
```

### Build & run: 
```
npm install
npm build
npm start
```

## Deployment
### Setup:
```
heroku login
heroku git:remote -a stockfish-wrapper
```

### Deploy:  
Commit changes, then:
```
npm run deploy
```
