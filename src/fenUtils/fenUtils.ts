import _ from 'lodash'
import { applyMove } from '../chessEngineService/chessEngineService'
import { Color, ParsedFen, MoveType, PieceCode } from '../types/types'

export function getTurnColor(fen: string) {
  const splitFen = fen.split(' ')
  return splitFen[1]
}

export function parseNewMove(oldFen: string, newPositions: number[][]) {
  const fenSegments = oldFen.split(' ')
  const piecePositions = fenSegments[0]
  const rows = piecePositions.split('/')
  const enPassantSpot = fenSegments[3]

  const oldPositions: number[][] = []
  rows.forEach((row, rowIndex) => {
    oldPositions.push([])
    for (const char of row) {
      if (!isNumber(char)) {
        if (char.toUpperCase() === char) {
          oldPositions[rowIndex].push(1)
        } else {
          oldPositions[rowIndex].push(2)
        }
      } else {
        for (let i = 0; i < parseInt(char); i++) {
          oldPositions[rowIndex].push(0)
        }
      }
    }
  })

  let potentialOrigins = []
  let potentialDestinations = []
  for (let i = 0; i < 8; i++) {
    for (let j = 0; j < 8; j++) {
      const newSpotOccupied = newPositions[i][j]
      const oldSpotOccupied = oldPositions[i][j]
      if (newSpotOccupied === oldSpotOccupied) {
        continue
      }
      if (!newSpotOccupied) {
        potentialOrigins.push(coordinatesFromIndeces(i, j))
      }
      if (newSpotOccupied) {
        potentialDestinations.push(coordinatesFromIndeces(i, j))
      }
    }
  }

  if (potentialDestinations.length === 2 && potentialOrigins.length === 2) {
    if (potentialDestinations.includes('g1')) return 'e1g1'
    if (potentialDestinations.includes('c1')) return 'e1c1'
    if (potentialDestinations.includes('g8')) return 'e8g8'
    if (potentialDestinations.includes('c8')) return 'e8c8'
  }

  console.log("oldFen: ", oldFen)
  console.log("newPositions: ", newPositions)

  console.log(potentialOrigins)
  console.log(potentialDestinations)

  const destinationsValid = potentialDestinations.length === 1
  const originsValid =
    enPassantSpot === '-'
      ? potentialOrigins.length === 1
      : potentialOrigins.length > 0 && potentialOrigins.length <= 2
  if (!destinationsValid || !originsValid) {
    throw 'newPositions incompatible with provided fen string'
  }

  let origin = potentialOrigins.length > 1 ? enPassantSpot : potentialOrigins[0]
  let destination = potentialDestinations[0]

  return origin + destination
}

export function coordinatesFromIndeces(row: number, col: number) {
  return `${(col + 10).toString(36)}${8 - row}`
}

export function indecesFromCoordinates(coordinates: string) {
  const col = coordinates.charAt(0).charCodeAt(0) - 97
  const row = 8 - parseInt(coordinates.charAt(1))

  return [row, col]
}

export async function getMoveType(
  fen: string,
  move: string
): Promise<MoveType> {
  const oldParsedFen = parseFen(fen)
  const newFen = await applyMove(fen, move)
  const newParsedFen = parseFen(newFen)

  const fromField = move.slice(0, 2)
  const toField = move.slice(2)

  // castle
  function hasCastled(color: Color): boolean {
    const kingCode = color === 'white' ? 'K' : 'k'
    const kingPos = findPieceLocation(fen, kingCode)

    if (!kingPos[0]) {
      throw 'Illegal state: Your board does not seem to contain both kings!'
    }
    if (kingPos[0] !== fromField) {
      return false
    }

    const fromFieldX = indecesFromCoordinates(fromField)[1]
    const toFieldX = indecesFromCoordinates(toField)[1]

    return Math.abs(fromFieldX - toFieldX) > 1
  }

  if (hasCastled('white') || hasCastled('black')) {
    if (toField.charAt(0) === 'c') {
      return 'castle_queen'
    }
    if (toField.charAt(0) === 'g') {
      return 'castle_king'
    }
  }

  // en passant
  if (toField === oldParsedFen.enPassantSpot) return 'en_passant'

  // takes
  const oldPieces = parsePieces(oldParsedFen)
  const newPieces = parsePieces(newParsedFen)

  if (oldPieces.length < newPieces.length) {
    throw 'Something went wrong: Piece count cannot increment as a result of a legal move.'
  }
  const differences = getDiff(oldPieces, newPieces)
  if (differences.length === 1 && move.length === 4) {
    return 'takes'
  }

  // promotes
  if (move.length === 5) {
    if (differences.length === 3) {
      return `takes_and_promotes_to_${move.charAt(4) as PieceCode}`
    } else {
      return `promotes_to_${move.charAt(4) as PieceCode}`
    }
  }

  return 'regular'
}

export function findPieceLocation(fen: string, pieceCode: PieceCode) {
  const locationsOutput: string[] = []

  const parsedFen = parseFen(fen)

  parsedFen.rows.forEach((row, rowIndex) => {
    let colOffset = 0
    for (let colIndex = 0; colIndex < row.length; colIndex++) {
      const char = row.charAt(colIndex)

      if (char === pieceCode) {
        locationsOutput.push(
          coordinatesFromIndeces(rowIndex, colIndex + colOffset)
        )
      }

      if (isNumber(char)) {
        const offset = parseInt(char)
        colOffset += offset - 1
      }
    }
  })
  return locationsOutput
}

export function parsePieces(parsedFen: ParsedFen): PieceCode[] {
  const pieces: PieceCode[] = []
  parsedFen.rows.forEach((row) => {
    for (let i = 0; i < row.length; i++) {
      const char = row.charAt(i)
      if (!isNumber(char)) pieces.push(char as PieceCode)
    }
  })
  return pieces
}

export function parseFen(fen: string): ParsedFen {
  const fenSegments = fen.split(' ')
  const piecePositions = fenSegments[0]
  const rows = piecePositions.split('/')
  const enPassantSpot = fenSegments[3]

  return { rows: rows, enPassantSpot: enPassantSpot }
}

export function isNumber(input: string) {
  return !isNaN(parseInt(input))
}

function getDiff<T>(arr1: T[], arr2: T[]) {
  const output: T[] = []

  const occurences = new Map<T, number>()
  arr1.forEach((item) => {
    const currentCount = occurences.get(item)
    if (!currentCount) {
      occurences.set(item, 1)
    } else {
      occurences.set(item, currentCount + 1)
    }
  })
  arr2.forEach((item) => {
    const currentCount = occurences.get(item)
    if (!currentCount) {
      occurences.set(item, -1)
    } else {
      occurences.set(item, currentCount - 1)
    }
  })

  occurences.forEach((value, key) => {
    _.range(Math.abs(value)).forEach(() => {
      output.push(key)
    })
  })

  return output
}
