import { MoveType } from '../types/types'
import { getMoveType } from './fenUtils'

describe('getMoveType', () => {
  test('regular', async () => {
    const regular = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
    const moveType = await getMoveType(regular, 'e2e4')
    expect(moveType).toBe('regular' as MoveType)
  })
  test('promotesToQ', async () => {
    const promotesToQ = '8/7P/2k5/8/8/8/8/3K4 w - - 0 1'
    const moveType = await getMoveType(promotesToQ, 'h7h8q')
    expect(moveType).toBe('promotes_to_q' as MoveType)
  })
  test('promotesToN', async () => {
    const promotesToN = '8/5P1k/8/7K/2B5/2B5/8/8 w - - 0 1'
    const moveType = await getMoveType(promotesToN, 'f7f8n')
    expect(moveType).toBe('promotes_to_n' as MoveType)
  })
  test('takesAndPromotesToQ', async () => {
    const takesAndPromotesToQ = '6q1/7P/2k5/8/8/8/8/3K4 w - - 0 1'
    const moveType = await getMoveType(takesAndPromotesToQ, 'h7g8q')
    expect(moveType).toBe('takes_and_promotes_to_q')
  })
  test('castles', async () => {
    const castles =
      'r1bqkb1r/pppp1ppp/2n2n2/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R w KQkq - 4 4'
    const moveType = await getMoveType(castles, 'e1g1')
    expect(moveType).toBe('castle_king')
  })
  test('enPassant', async () => {
    const enPassant = '1k4r1/8/8/5pP1/8/8/8/4K3 w - f6 0 2'
    const moveType = await getMoveType(enPassant, 'g5f6')
    expect(moveType).toBe('en_passant')
  })
  test('takes', async () => {
    const takes = 'rnbqkbnr/ppp1pppp/8/3p4/4P3/8/PPPP1PPP/RNBQKBNR w KQkq - 0 2'
    const moveType = await getMoveType(takes, 'e4d5')
    expect(moveType).toBe('takes')
  })
})