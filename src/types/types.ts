export type PieceCode =
  | 'p'
  | 'r'
  | 'n'
  | 'b'
  | 'k'
  | 'q'
  | 'P'
  | 'R'
  | 'N'
  | 'B'
  | 'K'
  | 'Q'

export type MoveType =
  | 'castle_queen'
  | 'castle_king'
  | 'takes'
  | 'en_passant'
  | `takes_and_promotes_to_${PieceCode}`
  | `promotes_to_${PieceCode}`
  | 'regular'

export type Color = 'white' | 'black'

export type ParsedFen = {
  rows: string[]
  enPassantSpot: string
}
