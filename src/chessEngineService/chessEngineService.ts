import { spawn } from 'child_process'
import { parseNewMove } from '../fenUtils/fenUtils'
import { runStockfish, stockfishPath } from '../stockfishWrapper/stockfishWrapper'

export async function getMoveForFenAsync(
  fen: string,
  depth: number
): Promise<string> {
  const commands = [`position fen ${fen}`, `go depth ${depth}`]

  const resolveCondition = (outputLine: string) =>
    outputLine.includes('bestmove')

  const outputTransformation = (outputLine: string) => {
    const outputStartIndex = outputLine.indexOf('bestmove') + 'bestmove '.length
    const outputEndIndex = outputStartIndex + 5
    return outputLine.slice(outputStartIndex, outputEndIndex).trim()
  }

  return runStockfish(commands, resolveCondition, outputTransformation)
}

export async function convertFen(
  oldFen: string,
  newPositions: number[][]
): Promise<string> {
  const newMove = parseNewMove(oldFen, newPositions)

  const legalMoves = await getLegalMoves(oldFen)
  console.log("legalMoves: ", legalMoves)

  if (!legalMoves.includes(newMove)) {
    throw 'move not legal'
  }

  const commands = [`position fen ${oldFen} moves ${newMove}`, 'd']

  const resolveCondition = (outputLine: string) => outputLine.includes('Fen: ')

  const outputTransformation = (outputLine: string) => {
    const outputStartIndex = outputLine.indexOf('Fen: ') + 'Fen: '.length
    const outputEndIndex = outputLine.indexOf('\n', outputStartIndex)
    return outputLine.slice(outputStartIndex, outputEndIndex)
  }

  return runStockfish(commands, resolveCondition, outputTransformation)
}

export async function getLegalMoves(fen: string): Promise<string[]> {
  const commands = [`position fen ${fen}`, 'go perft 5']

  return new Promise((resolve, reject) => {
    const legalMoves: string[] = []

    // INIT
    const child = spawn(stockfishPath)

    // STDIN
    child.stdin.setDefaultEncoding('utf-8')
    commands.forEach((command) => {
      child.stdin.write(`${command}\n`)
    })

    // STDOUT EVENT HANDLING
    child.stdout.on('data', (data: string) => {
      const dataString = data.toString()
      console.log('dataString: ', dataString)
      console.log('isMove: ', dataString.includes(':') && !dataString.includes('Nodes searched:'))
      console.log('isEndCondition: ', dataString.includes('Nodes searched:'))
      const strings = dataString.split('\n')
      strings.forEach((string) => {
        if (string.includes(':') && !string.includes('Nodes searched:')) {
          console.log('add')
          legalMoves.push(string.slice(0, 4))
        }
        if (string.includes('Nodes searched:')) {
          console.log('resolve')
          resolve(legalMoves)
        }
      })
    })

    // RUNTIME ERROR EVENT HANDLING
    child.stderr.on('data', (data) => {
      console.error(`stderror: ${data}`)
      reject(`We have encountered a runtime error: ${data}`)
    })

    // STARTUP ERROR EVENT HANDLING
    child.on('error', (error) => {
      console.error(`error: ${error}`)
      reject(`There has been a problem with starting stockfish: ${error}`)
    })

    // EXIT EVENT HANDLING
    child.on('exit', (code, signal) => {
      if (code) {
        reject(`Process exited with code: ${code}`)
      }
      if (signal) {
        reject(`Process killed with signal: ${signal}`)
      }
      console.info(`Done!`)
    })
  })
}

export async function applyMove(oldFen: string, newMove: string) {
  const commands = [`position fen ${oldFen} moves ${newMove}`, 'd']

  const resolveCondition = (outputLine: string) => outputLine.includes('Fen: ')

  const outputTransformation = (outputLine: string) => {
    const outputStartIndex = outputLine.indexOf('Fen: ') + 'Fen: '.length
    const outputEndIndex = outputLine.indexOf('\n', outputStartIndex)
    return outputLine.slice(outputStartIndex, outputEndIndex)
  }

  return runStockfish(commands, resolveCondition, outputTransformation)
}
