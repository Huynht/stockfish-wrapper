import { getLegalMoves } from "./chessEngineService"

jest.setTimeout(10 * 1000);

describe('checkMoveLegality', () => {
    jest.setTimeout(10 * 1000);

    test('white after startpos', async () => {
        jest.setTimeout(10 * 1000);
        const fen = 'rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1'
        const expectedMoves = ['a2a3',
            'b2b3',
            'c2c3',
            'd2d3',
            'e2e3',
            'f2f3',
            'g2g3',
            'h2h3',
            'a2a4',
            'b2b4',
            'c2c4',
            'd2d4',
            'e2e4',
            'f2f4',
            'g2g4',
            'h2h4',
            'b1a3',
            'b1c3',
            'g1f3',
            'g1h3',]

        const legalMoves = await getLegalMoves(fen)

        legalMoves.forEach((legalMove) => {
            if (!expectedMoves.includes(legalMove)) {
                console.log('jsfjoisdfiojsdfijsdfj')
                console.log(legalMove)
            }
        })
        if (legalMoves.length !== expectedMoves.length) {
            console.log('size unequal')
            console.log('legal moves')
            console.log(legalMoves.length)
            console.log(legalMoves)
            console.log('expectedmoves')
            console.log(expectedMoves)
            console.log(expectedMoves.length)
        }

        legalMoves.forEach((legalMove) => {
            expect(expectedMoves.includes(legalMove)).toBeTruthy()
        })
        expect(legalMoves.length === expectedMoves.length).toBeTruthy()
    })

    test('black after e2e4', async () => {
        jest.setTimeout(10 * 1000);

        const fen = 'rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq - 0 1'
        const expectedMoves = ['a7a6',
            'b7b6',
            'c7c6',
            'd7d6',
            'e7e6',
            'f7f6',
            'g7g6',
            'h7h6',
            'a7a5',
            'b7b5',
            'c7c5',
            'd7d5',
            'e7e5',
            'f7f5',
            'g7g5',
            'h7h5',
            'b8a6',
            'b8c6',
            'g8f6',
            'g8h6',]

        const legalMoves = await getLegalMoves(fen)


        legalMoves.forEach((legalMove) => {
            if (!expectedMoves.includes(legalMove)) {
                console.log('jsfjoisdfiojsdfijsdfj')
                console.log(legalMove)
            }
        })
        if (legalMoves.length !== expectedMoves.length) {
            console.log('size unequal')
            console.log(legalMoves.length)
            console.log(expectedMoves.length)
        }

        legalMoves.forEach((legalMove) => expect(expectedMoves.includes(legalMove)).toBeTruthy())
        expect(legalMoves.length === expectedMoves.length)
    })
})