import express, { Express, Request, Response } from 'express'
import bodyParser from 'body-parser'
import {
  indecesFromCoordinates,
  getMoveType,
  getTurnColor,
  coordinatesFromIndeces,
} from './fenUtils/fenUtils'
import {
  getMoveForFenAsync,
  convertFen,
  applyMove,
} from './chessEngineService/chessEngineService'

// INIT
const app: Express = express()
const port = process.env.PORT || 3005

// PLUGINS
app.use(bodyParser.json())

// ROUTES
app.get('/', (req, res) => {
  console.info('GET: /')
  res.send('This is the stockfish wrapper')
})

app.post('/best-move', async (req: Request, res: Response) => {
  console.info('POST: /best-move')

  const body: {
    fen: string
    depth: number
  } = req.body

  if (body) {
    try {
      const bestMove = await getMoveForFenAsync(body.fen, body.depth)
      console.info('best move: ' + bestMove)

      const moveType = await getMoveType(body.fen, bestMove)
      console.info('moveType: ' + moveType)

      res.send({
        bestMove: bestMove.slice(0, 4),
        moveType: moveType,
      })
    } catch (e) {
      console.error(e)
      res.status(400).send(e)
    }
  } else {
    res.status(400).send('No Body')
  }
})

app.post('/apply-move', async (req: Request, res: Response) => {
  console.info('POST: /apply-move')

  const body: {
    fen: string
    move: string
  } = req.body

  if (body) {
    try {
      const nextFen = await applyMove(body.fen, body.move)
      res.send({ fen: nextFen })
    } catch (e) {
      console.error(e)
      res.status(400).send(e)
    }
  } else {
    res.status(400).send('No Body')
  }
})

app.post('/is-game-over', async (req: Request, res: Response) => {
  console.info('POST: /is-game-over')

  const body: {
    fen: string
  } = req.body

  if (body) {
    try {
      const bestMove = await getMoveForFenAsync(body.fen, 5)
      console.log(bestMove)
      if (bestMove === '(none') {
        console.info('game is over: ' + body.fen)
        if (getTurnColor(body.fen) === 'b') {
          res.send({
            isOver: true,
            winner: 'white',
          })
        } else {
          res.send({
            isOver: true,
            winner: 'black',
          })
        }
      } else {
        console.info('game is not over: ' + body.fen)
        res.send({
          isOver: false,
          winner: undefined,
        })
      }
    } catch (e) {
      console.error(e)
      res.status(400).send(e)
    }
  } else {
    res.status(400).send('No Body')
  }
})

app.post('/convert-fen', async (req: Request, res: Response) => {
  console.info('POST: /convert-fen')

  const body: {
    oldFen: string
    // newPositions encoding: 0 = empty, 1 = white / yellow,  2 = black / blue
    newPositions: number[][]
  } = req.body

  if (body) {
    try {
      const nextFen = await convertFen(body.oldFen, body.newPositions)
      console.info('next fen: ' + nextFen)
      res.send({ nextFen: nextFen })
    } catch (e) {
      console.error(e)
      res.status(400).send(e)
    }
  } else {
    res.status(400).send('No Body')
  }
})

app.post('/convert-position', async (req: Request, res: Response) => {
  console.info('POST: /convert-position')

  const body: {
    coordinates?: string
    indeces?: [number, number]
  } = req.body

  if (body.coordinates) {
    res.send({ indeces: indecesFromCoordinates(body.coordinates) })
  }
  if (body.indeces) {
    res.send({
      coordinates: coordinatesFromIndeces(body.indeces[0], body.indeces[1]),
    })
  }

  res.status(400).send('Invalid body')
})

// LISTEN ON PORT
app.listen(port, () => {
  console.log(`⚡️[server]: Server is running at port:${port}`)
})
