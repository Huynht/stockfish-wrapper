import { spawn } from 'child_process'

export const stockfishPath = process.env.STOCKFISH_PATH || '/usr/games/stockfish'

async function runStockfish(
  commands: string[],
  resolveCondition: (outputLine: string) => boolean,
  outputTransformation?: (outputLine: string) => string
): Promise<string> {
  return new Promise((resolve, reject) => {
    // INIT
    const child = spawn(stockfishPath)

    // STDIN
    child.stdin.setDefaultEncoding('utf-8')
    commands.forEach((command) => {
      child.stdin.write(`${command}\n`)
    })

    // STDOUT EVENT HANDLING
    child.stdout.on('data', async (data: string) => {
      const dataString = data.toString()
      if (resolveCondition(dataString)) {
        const output = outputTransformation
          ? outputTransformation(dataString)
          : dataString
        resolve(output)
      }
    })

    // RUNTIME ERROR EVENT HANDLING
    child.stderr.on('data', (data) => {
      console.error(`stderror: ${data}`)
      reject(`We have encountered a runtime error: ${data}`)
    })

    // STARTUP ERROR EVENT HANDLING
    child.on('error', (error) => {
      console.error(`error: ${error}`)
      reject(`There has been a problem with starting stockfish: ${error}`)
    })

    // EXIT EVENT HANDLING
    child.on('exit', (code, signal) => {
      if (code) {
        reject(`Process exited with code: ${code}`)
      }
      if (signal) {
        reject(`Process killed with signal: ${signal}`)
      }
      console.info(`Done!`)
    })
  })
}

export { runStockfish }
