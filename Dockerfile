FROM ubuntu:latest
RUN apt-get update
RUN apt-get install -y stockfish
RUN apt-get install -y curl
RUN curl -fsSL https://deb.nodesource.com/setup_current.x | bash -
RUN apt-get install -y nodejs
WORKDIR /app
COPY . .
RUN npm run build
RUN curl --help
RUN find / -type f -name stockfish
CMD ["node", "dist/app.js"]
EXPOSE 3005